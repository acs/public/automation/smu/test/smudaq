%% Init

clear, clc
addpath('func')

t = initTCP('134.130.169.27', 7080);
s = initDAQ(1e6, 1e4, 'sync_none','debug_spi');

%% Output Signal

createLinear(s,20);                                                        % Linear sweep

%% Acquisition

confSMU(t, s);
sendSMU(t, '[Start]', 'keepalive');

queueOutputData(s, s.UserData.dac);                                        % Queue data in the output buffer
startBackground(s);
s.wait()

while(t.UserData.samples.read < t.UserData.samples.total)
    pause(0.5);
end
sendSMU(t, '[Stop]');
outputSingleScan(s,[0 0]);

data.smu = double(reshape(t.UserData.buff_data, 8, t.UserData.sample_tot))'/2^15*10;
data.daq = s.UserData.dac(1:s.UserData.Fs.ratio:end);
data.Fs = s.UserData.Fs;
data.t = s.UserData.t;

%% Processing

for ch = 1:8
    mdl{ch} = fitlm(data.daq, data.smu(:,ch));                           % Linear regression
end

%% Plot

figure
plot(dac.ao(1:data.Fs.ratio:end), data.smu)
xlabel('DAQ [V]')
ylabel('SMU [V]')


plotDebugSPI(s);

%% Save

save('dataLinear_10k_2.mat','data','t_smu','Fs_smu','Fs_daq','F','Nc','mdl')