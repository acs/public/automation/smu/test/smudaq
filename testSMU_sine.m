%% Init

clear, clc
addpath('func')

t = initTCP('134.130.169.19', 7080);
p = initSMU(1e4, 10, 'sync_pps', 'mode_freerun');
s = initDAQ(1e6, 'Dev1', p, 'debug_none');

%% Test procedure

createSine(s,50,2);                                                        % Create sine wave at 50 Hz

confSMU(t, s);
sendSMU(t, '[Start]', 'keepalive');
pause(0.5)

  
queueOutputData(s, s.UserData.dac);                                        % Queue data in the output buffer
pause(0.1);

startBackground(s);
s.wait();tic
while (toc < s.UserData.T)
    pause(0.5);
end

% Retrieve data
data = readSMU(t, s);


% sendSMU(t, '[Stop]');
% sendSMU(t, '[Exit]');

%% Plot

figure
plot(data.t.smu, data.smu(:,1),...
     data.t.smu, data.daq);
xlabel('Time [s]')
ylabel('Voltage [V]')
legend('SMU', 'DAQ')

plotDebugSMU(data);
% plotDebugSPI(s);

%% Save

% save('data_sine.mat','data')

