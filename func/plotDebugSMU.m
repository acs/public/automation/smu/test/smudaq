function plotDebugSMU(data)
%plotDebugSMU Plot derivative based SMU acquisition debug
%   @s      obj, DAQ session object

t = data.t.smu;
D = diff(data.smu(:,1));
D = D/max(D);

figure
plot(data.t.smu,[D;0],...
     data.t.smu, data.daq(:,2))
xlabel('Time [s]')
end

