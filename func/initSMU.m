function [p] = initSMU(Fs, Fr, sync, mode)
%initSMU Init the SMU board configuration
%   @Fs     value, SMU timebase frequency
%   @Fr     value, SMU reporting rate
%   @sync   None, no connection
%           PPS,  GPS connection
%           NTP,  Time server
%           Debug,external timebase
%   @mode   singleshot, on trigger
%           freerun,  continuous trigger

%% @Fs

if (Fs < 1e3)
    error('Valid SMU sampling rates are > 1 kHz.');
elseif(Fs > 2e4)
    error('Valid SMU sampling rates are < 20 kHz.');
else
    p.Fs = Fs;
end

%% @Fr

if (Fr < 1)
    error('Valid SMU reporting rates are > 1 Hz.');
elseif(Fr > 100)
    error('Valid SMU reporting rates are < 100 Hz.');
else
    p.Fr = Fr;
end

%% @sync

p.sync.sval = sync;

switch sync
    case 'sync_none'
        p.sync.nval = 0;
    case 'sync_pps'
        p.sync.nval = 1;
    case 'sync_ntp'
        p.sync.nval = 2;
    case 'sync_debug'
        p.sync.nval = 3;
    otherwise
        error('Valid SYNC values are ''sync_none'', ''sync_pps'', ''sync_ntp'' and ''sync_debug''.');
end

%% @sync

p.mode.sval = mode;

switch mode
    case 'mode_singleshot'
        p.mode.nval = 0;
    case 'mode_freerun'
        p.mode.nval = 1;
    otherwise
        error('Valid MODE values are ''mode_singleshot'', ''mode_freerun''.');
end

end