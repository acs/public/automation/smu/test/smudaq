function [data] = readSMU(t, s)
%readPMU Read the PMU data
%   @t      obj, TCP session object
%   @s      obj, DAQ session object

if (t.UserData.sample_cnt ~= t.UserData.sample_tot)
    warning('Data acquisition incomplete.');
    data.smu = nan(s.UserData.T*s.UserData.Fs.smu,8);
else
    disp('Data acquisition complete.');
    data.smu = double(reshape(t.UserData.buff_data, 8, t.UserData.sample_cnt))'/2^15*10;
end

data.daq = s.UserData.dac(1:s.UserData.Fs.ratio:end,:);
data.Fs = s.UserData.Fs;
data.t = s.UserData.t;
end

