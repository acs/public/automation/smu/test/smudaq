function [dac] = createSine(s,F,T)
%createSine Generates DAC signals for a sine test
%   @s      obj, DAQ session object
%   @F      value, Frequency of the sine wave
%   @T      value, Singleshot test in seconds
%           inf, Continuous test

if (F < 0.1)
    error('Valid signal frequency values are > 100 mHz.');
elseif (F > s.UserData.Fs.smu/2)
    error(['Valid signal frequency values are < ' num2str(s.UserData.Fs.smu/2/1e3)' kHz.']);
end

if (T < 1e-2)
    error('Valid signal duration values are > 10 ms.');
elseif ((T > 10) && ~isinf(T))
    error('Valid signal duration values are < 10 s.');
end

if (isinf(T))
    if (s.UserData.mode.sval ~= "mode_freerun")
        error('SMU continuous acquisition requires ''mode_freerun''');
    end
    T = 1;
else

Fs = s.UserData.Fs;

Fs.ratio = Fs.daq/Fs.smu;                                                  % Sampling frequency ratio

Ts.daq = 1/Fs.daq;                                                         % DAQ Sampling period
Ts.smu = 1/Fs.smu;                                                         % SMU Sampling Period

t.daq = (0:Ts.daq:T-Ts.daq)';                                              % DAQ Timebase
t.smu = (0:Ts.smu:T-Ts.smu)';                                              % SMU Timebase

L.daq = length(t.daq);                                                     % DAQ Number of samples
L.smu = length(t.smu);                                                     % SMU Number of samples


dac.ao = 9*sin(2*pi*F*t.daq);                                              % -10/10 Ramp signal
dac.do = repmat([ones(1e3,1);zeros(Fs.daq-1e3,1)],ceil(T),1);              % PPS signal  
dac.do = dac.do(1:L.daq);

s.UserData.t = t;
s.UserData.L = L;
s.UserData.T = T;
s.UserData.Fs = Fs;
s.UserData.dac = [dac.ao dac.do];
s.Rate = Fs.daq;                                                           % DAQ rate
end